#!/usr/bin/env node

/* Nightmare based screenshot utility
 *
 * Author: Ben Lopatin
 * Copyright: Ben Lopatin, 2016
 * License: MIT
 *
 */
var Nightmare = require('nightmare');
var async = require('async');
var slug = require('slug');
var YAML = require('yamljs');
var path = require('path');
var argv = require('minimist')(process.argv.slice(2), {
    alias: {'password': 'p', 'username': 'u', 'config': 'c'},
    default: {'config': '', 'path': '.'}
});


if(argv.config === '') {
    console.log('No config file! Specify a YAML config file path with the -c option');
    process.exit(1);
}

const pageConfig = YAML.load(argv.config);
const useragent = pageConfig.useragent || "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36";


// Returns a slugified version of the string, with "home" as the default
// return value for empty slugified strings.
slugify = function(str) {
    const result = slug(str);
    if (result === '') return 'home';
    return result;
};


// Maps screenshots for a single URL out against dimensions
const genScreenshots = (nightmare, url) =>
    // Mappable function
    function(data, callback) {
        nightmare.viewport(data.width, data.height)
            .screenshot(path.join(argv.path, slugify(url) + "-" + data.name + ".png"));
    }
;


// Maps screenshots out across URLs
const screenshotURL = function(url, callback){
    const nightmare = new Nightmare();
    nightmare.authentication(argv.username, argv.password)
        .useragent(useragent)
        .goto(pageConfig.host + url)
        .wait();
    doScreenshots = genScreenshots(nightmare, url);
    async.map(pageConfig.dimensions, doScreenshots, function() {
        // Error handling?
    });
    nightmare.run(function (err, nightmare) {
       if (err) return console.log(err);
        console.log('Finished with', url);
    }).end();
};

async.map(pageConfig.urls, screenshotURL, function(){
    // Error handling?
});

