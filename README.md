# Screenshooter.js

A Nightmare backed screenshot utility.

### Usage

    node index.js -c path-to-config.yaml

You will need to provide a configuration in YAML format to build all of
your screenshots. E.g.

    host: "https://mydomain.com"
    dimensions:
        - name: iwatch5
          width: 50
          height: 50
        - name: desktop
          width: 1024
          height: 800
    urls:
        - /path/
        - /another/path/

Optionally you can provide a username and password for HTTP basic auth
using the `-u`/`--username` and `p`/`--password` parameters.

    node index.js -c path-to-config.yaml -u tester -p keepout

### Options

    `-c/--config`
        path to the YAML configuration file

    `--path`
        path for screenshot output (default is current directory)

    `-u/--username`
        username for HTTP basic auth

    `-p/--password`
        password for HTTP basic auth

### License and copyright

&copy; 2016 Ben Lopatin

MIT License
